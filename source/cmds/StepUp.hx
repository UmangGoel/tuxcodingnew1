/*
 * Copyright 2013 Tux4Kids 
 * Distributed under the terms of the MIT License. 
 * 
 * Author(s): 
 * Abdelhakim Deneche 
 */

package cmds;

import tileobjs.TileObj;
import tileobjs.Crate;
import tileobjs.Coin;
import tileobjs.Key;
/**
 * make the character jump up over a crate in the same column
 */
class StepUp extends Cmd
{
	
	public function new(world:World) 
	{
		super(world);
	}
	
	override public function run():Bool
	{
		world.player.stepUp();
		return false;
	}
	
	override public function canRun():Bool 
	{
		var player:Player = world.player;

		// check the target tile is inside the map and (empty or it has a coin or it has a key)
		/*		if (!world.insideMapAndEmpty(player.tileX, player.tileY-1)) return false;	  "CHANGED"		*/
		if(!world.insideMap(player.tileX,player.tileY-1)) return false;
		if(! ( world.isEmpty(player.tileX,player.tileY-1) || Std.is(world.getObject(player.tileX,player.tileY-1) ,Coin) || Std.is(world.getObject(player.tileX,player.tileY-1) ,Key) )) return false;
		
		// player's tile must contain a crate
		var obj:TileObj = world.getObject(player.tileX, player.tileY);
		if (obj != null && Std.is(obj, Crate)) return true;
		return false;
	}
	
}